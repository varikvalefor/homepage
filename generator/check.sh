#!/usr/bin/env bash

site="${1:-_site}"

# docs/ seems to be a URL rewrite rule provided by haskell.org
# dist/mac_frameworks appears to be lost to the sands of time.
linkchecker "$site" \
  --ignore-url 'docs/' \
  --ignore-url=dist/mac_frameworks \

