{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/fe56507bd3063a30f3a741a45bf3ba74a91cfac2";
  };
  outputs = { self, nixpkgs }: 
    let pkgs = import nixpkgs { system = "x86_64-linux"; };
    in {
      apps.x86_64-linux.generator = {
        type = "app";
        program = "${self.packages.x86_64-linux.generator}/bin/ghc-homepage";
      };
      apps.x86_64-linux.check = {
        type = "app";
        program = "${self.packages.x86_64-linux.scripts}/bin/check.sh";
      };

      packages.x86_64-linux.generator =
        (pkgs.haskellPackages.callCabal2nix "ghc-homepage" ./generator { }).overrideAttrs (oldAttrs: {
          nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ pkgs.makeWrapper ];
          postInstall = ''
            wrapProgram $out/bin/ghc-homepage \
              --set LOCALE_ARCHIVE "${pkgs.glibcLocales}/lib/locale/locale-archive" \
              --set LANG en_US.UTF-8
          '';
        });

      packages.x86_64-linux.scripts = with pkgs;
        stdenv.mkDerivation {
          name = "ghc-homepage-scripts";
          buildInputs = [ linkchecker ];
          nativeBuildInputs = [ makeWrapper ];
          src = ./generator;
          installPhase = ''
            mkdir -p $out/bin
            install check.sh $out/bin
            chmod ugo+rx $out/bin/check.sh
            wrapProgram $out/bin/check.sh \
              --prefix PATH : ${linkchecker}/bin:$out/bin
          '';
        };

      packages.x86_64-linux.site = with pkgs;
        stdenv.mkDerivation {
          name = "site";
          nativeBuildInputs = [ self.packages.x86_64-linux.generator ];
          src = ./.;
          buildPhase = "ghc-homepage build";
          installPhase = ''
            mkdir -p $out
            cp -R _site/* $out
          '';
        };

      devShell.x86_64-linux =
        pkgs.mkShell {
          nativeBuildInputs = [
            self.packages.generator
            self.packages.scripts
          ];
        };
    };
}

