---
author: bgamari
title: "GHC 8.0.2 is available!"
date: 2017-01-11
tags: release
---

The GHC team is happy to at last announce the 8.0.2 release of the
Glasgow Haskell Compiler. Source and binary distributions are available
at

    http://downloads.haskell.org/~ghc/8.0.2/

This is the second release of the 8.0 series and fixes nearly
two-hundred bugs. These include,

  * Interface file build determinism (#4012).

  * Compatibility with macOS Sierra and GCC compilers which compile 
    position-independent executables by default

  * Compatibility with systems which use the gold linker

  * Runtime linker fixes on Windows (see #12797)

  * A compiler bug which resulted in undefined reference errors while
    compiling some packages (see #12076)

  * A number of memory consistency bugs in the runtime system

  * A number of efficiency issues in the threaded runtime which manifest
    on larger core counts and large numbers of bound threads.

  * A typechecker bug which caused some programs using
    -XDefaultSignatures to be incorrectly accepted.

  * More than two-hundred other bugs. See [Trac](https://ghc.haskell.org/trac/ghc/query?status=closed&milestone=8.0.2&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=component&order=priority) for a complete
    listing.

  * #12757, which lead to broken runtime behavior and even crashes in
    the presence of primitive strings.

  * #12844, a type inference issue affecting partial type signatures.

  * A bump of the `directory` library, fixing buggy path
    canonicalization behavior (#12894). Unfortunately this required a
    major version bump in `directory` and minor bumps in several other
    libraries.

  * #12912, where use of the `select` system call would lead to runtime
    system failures with large numbers of open file handles.

  * #10635, wherein -Wredundant-constraints was included in the -Wall
    warning set

A more detailed list of the changes included in this release can be
found in the [release notes](https://downloads.haskell.org/~ghc/8.0.2/docs/html/users_guide/8.0.2-notes.html).

Please note that this release breaks with our usual tendency to avoid
major version bumps of core libraries in minor GHC releases by including
an upgrade of the `directory` library to 1.3.0.0.

Also note that, due to a rather serious bug (#13100) affecting Windows
noticed late in the release cycle, the Windows binary distributions were
produced using a slightly [patched](http://downloads.haskell.org/~ghc/8.0.2/0001-SysTools-Revert-linker-flags-change.patch) source tree. Users compiling from
source for Windows should be certain to include this patch in their
build.

This release is the result of six months of effort by the GHC
development community. We'd like to thank everyone who has contributed
code, bug reports, and feedback to this release. It's only due to
their efforts that GHC remains a vibrant and exciting project.

 


## How to get it

Both the source tarball and binary distributions for a wide variety of platforms
are available [here](http://www.haskell.org/ghc/).


## Background

Haskell is a standardized lazy functional programming language.

The Glasgow Haskell Compiler (GHC) is a state-of-the-art programming suite for
Haskell. Included is an optimising compiler generating efficient code for a
variety of platforms, together with an interactive system for convenient, quick
development. The distribution includes space and time profiling facilities, a
large collection of libraries, and support for various language extensions,
including concurrency, exceptions, and foreign language interfaces. GHC is
distributed under a BSD-style open source license.


## Supported Platforms

The list of platforms we support, and the people responsible for them, can be
found on the [GHC wiki](https://gitlab.haskell.org/ghc/ghc/wikis/Platforms)


Ports to other platforms are possible with varying degrees of difficulty. The
[Building Guide](https://gitlab.haskell.org/ghc/ghc/wikis/Building) describes how to go about porting to a new platform.


## Developers

We welcome new contributors. Instructions on getting started with hacking on GHC
are available from GHC's [developer site](http://ghc.haskell.org/trac/ghc/).


## Community Resources

There are mailing lists for GHC users, develpoers, and monitoring bug tracker
activity; to subscribe, use the Mailman
[web interface](http://mail.haskell.org/cgi-bin/mailman/listinfo).

There are several other Haskell and GHC-related mailing lists on
[haskell.org](http://www.haskell.org); for the full list, see the
[lists page](https://mail.haskell.org/cgi-bin/mailman/listinfo).

Some GHC developers hang out on the `#ghc` and `#haskell` of the Freenode IRC
network, too. See the [Haskell wiki](http://www.haskell.org/haskellwiki/IRC_channel) for details.

Please report bugs using our bug tracking system. Instructions on reporting bugs
can be found [here](http://www.haskell.org/ghc/reportabug).
