---
author: thoughtpolice
title: "GHC Weekly News - 2015/02/17"
date: 2015-02-18
tags: ghc news
---

Hi \*,

It's time for the GHC weekly news. It's been particularly quiet the past week
still, and the `ghc-7.10` branch has been quite quiet. So the notes are
relatively short this week.

This week, GHC HQ met up to discuss some new stuff:

 - Most of the discussion this week was about particular bugs for GHC 7.10, including getting some tickets fixed like #10058, #8276, and #9968.
 - Since the 7.10 release is getting close, we'll be starting up a new status page about GHC 7.12 (and probably get started writing things for the HCAR report in May) and what our plans are soon. Watch this space!

As usual, we've had a healthy amount of random assorted chatter on the mailing lists:

 - Simon Peyton Jones opened the polls for the GHC 7.10 Prelude changes this week, following the discussions and delay of the 7.10 release, as to what the new Prelude should look like. Simon's email has all the details - and voting ends next week! <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008290.html>
 - Hengchu Zhang popped up on the list as an excited new contributor, and wanted to know about the process strategy for fixing a bug. Joachim was quick to respond with help - and welcome Hengchu! <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008324.html>
 - Francesco Mazzoli has a question about Template Haskell, specifically the semantics of reification since 7.8. In short, the semantics of `reify` changed in 7.8, and Francesco was wondering if the old behavior should be supported. But while it could be, it discussion seems to indicate that perhaps it shouldn't. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008327.html>
 - Darren Grant popped up on the list and asked: "I notice there are a series of related long-standing issues subject to particular cygwin64 quirks, and I'd like to offer time to help resolve these if possible". Darren wanted some pointers, and they were given! GHC on Windows crucially still needs dedicated developers; the email sparked up a bunch of chatter amongst Windows developers on the list as well, so hopefully life is coming back to it. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008333.html>
 - Jan Stolarek hit a confusing error when trying to install `vector` with HEAD and asked for help. The quick reply: you need support for the new `deepseq` package, which hasn't been merged upstream yet. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008349.html>
 - Francesco Mazzoli had a simple feature request: could we have anonymous FFI calls that don't require a name? <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008300.html>

Some noteworthy commits that went into `ghc.git` in the past week include:

 - Commit e22282e5d2a370395535df4051bdeb8213106d1c - GHC 7.12 will no longer ship with the `Typeable.h` header file.
 - Commit 5d5abdca31cdb4db5303999778fa25c4a1371084 - The LLVM backend has been overhauled and updated to use LLVM 3.6 exclusively.

Closed tickets the past week include: #10047, #10082, #10019, #10007, #9930, #10085, #10080, #9266, #10095, and #3649.
