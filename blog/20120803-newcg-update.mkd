---
author: simonmar
title: "The new code generator is nearly ready to go live"
date: 2012-08-03
tags: 
---

I've just spent another week working on the Glorious New Code Generator, and I'm pleased to say it's now in a state that we can think about switching over.

Just to recap, the "new code generator" is a replacement for the STG-to-Cmm phase of the compiler, using a new intermediate representation based on [Hoopl](http://research.microsoft.com/en-us/um/people/simonpj/papers/c--/dfopt.pdf) , and separating out the mechanics of layout out stack frames from code generation.  It's mainly an architectural change with little visible benefit, but it will be a big improvement in terms of flexibility and modularity inside the compiler.  We'll be able to express many optimisations that were hard before, e.g. #1498.

Furthermore, many bits of cruft will go away, such as large chunks of the horrible `CoreToStg` pass, and the SRT analysis.  These things are replaced by much cleaner analyses of the generated code using Hoopl.

For more details on the new code generator see [wiki:Commentary/Compiler/NewCodeGen], although that page needs a lot of updating after my recent hacking.

The current status in more detail:

 * everything works, with the possible exception of profiling.  I've just got a stage2 compiler working, and the whole test suite passes with no new failures.

 * generated code is on average as good as before, sometimes better and sometimes worse.  I have a few examples of tight loops that get better with the new code gen, but of course for complex low-level loopy code LLVM will still be necessary to get really good code.  On the other hand, GHC can take advantage of its better knowledge about aliasing, and I expect to make more use of this in the future.  There's quite a bit of scope for generating better code and adding cool Hoopl-based optimisations later.  Things are definitely looking up for low-level performance in GHC.

 * compile times will get a bit worse, at least initially.  This is where I've been spending a lot of effort: when I started work on the new code generator it was adding more than 100% to compile times, and I've got that down to about 5% for optimised compilation, 10-15% for non-optimised.  Compile times will improve further when we switch over properly and we can start removing a lot of cruft.

We now have a 7.6 branch, so my plan is to fix the one remaining blocker (profiling) and then switch over.  There will no doubt be breakage that hasn't shown up so far, but we have until 7.8 to find and fix it.

**EDIT**: I should mention that the new code generator is the work of many people, including Norman Ramsey, Simon Peyton Jones, John Dias, Edward Yang and probably others I've forgotten.
