---
author: bgamari
title: "GHC 9.2.1-rc1 is now available"
date: 2021-08-07
tags: release
---

The GHC team is very happy to announce the availability of the
release cadidate of the 9.2.1 release. Binary distributions, source
distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.2.1-rc1).

GHC 9.2 will bring a number of exciting features including:

 * A native code generation backend for AArch64, significantly reducing
   compilation times on ARM platforms such as the Apple M1.

 * Many changes in the area of records, including the new
   `RecordDotSyntax` and `NoFieldSelectors` language extensions, as well
   as Support for `DuplicateRecordFields` with `PatternSynonyms`.

 * Introduction of the new `GHC2021` language extension set, giving
   users convenient access to a larger set of language extensions which
   have been long considered stable.

 * Merge of `ghc-exactprint` into the GHC tree, providing infrastructure
   for source-to-source program rewriting out-of-the-box.

 * Introduction of a `BoxedRep` `RuntimeRep`, allowing for polymorphism
   over levity of boxed objects (#17526)

 * Implementation of the `UnliftedDataTypes` extension, allowing users
   to define types which do not admit lazy evaluation ([proposal][UnliftedDataTypes])

 * The new [-hi profiling][] mechanism which provides significantly
   improved insight into thunk leaks.

 * Support for the `ghc-debug` out-of-process heap inspection library
   [ghc-debug][]

 * Support for profiling of pinned objects with the cost-centre profiler
   (#7275)

 * Introduction of Haddock documentation support in TemplateHaskell (#5467)

Thank you to GitHub, IOHK, the Zw3rk stake pool, Tweag I/O, Serokell,
Equinix, SimSpace, and other anonymous contributors whose on-going
financial and in-kind support has facilitated GHC maintenance and
release management over the years. Moreover, this release would not have
been possible without the hundreds of open-source contributors whose
work comprise this release.

As always, do give this a try and open a [ticket][] if you see anything amiss.

Happy testing,

- Ben


[apple-m1]: https://www.haskell.org/ghc/blog/20210309-apple-m1-story.html
[UnliftedDataTypes]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0265-unlifted-datatypes.rst
[-hi profiling]: https://well-typed.com/blog/2021/01/first-look-at-hi-profiling-mode/
[ghc-debug]: http://ghc.gitlab.haskell.org/ghc-debug/
[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new


